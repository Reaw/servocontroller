/* This in an example of how to use the library, where we connect 2 servos on
the arduino board that have to move simultaneously and follow a pattern */

#include <ServoController.h>

#define pinServo2 1

unsigned short slowTimeToReachSpeed = 5000U; //5 seconds to reach the max speed

/* creating a pattern */
int _patternSize = 8;
/* the servos will go from 70° -> 110° -> 50° -> 130° -> 30° -> 150° -> 10° -> 170° and loop */
unsigned short basicPattern[8] = {70, 110,
                                            50, 130,
                                            30, 150,
                                            10, 170};

/* declaring a first servoController without parameters this servoController will therefore
use default values for pin (pin0), timeToReachSpeed (5sec), and a default sweep Pattern*/
ServoController servoController1 = ServoController();
/* declaring a second servoControllers and setting the pin, the timeToReachSpeed, and the pattern directly in the constructor */
ServoController servoController2 = ServoController(pinServo2, slowTimeToReachSpeed, basicPattern, _patternSize);

/*** SETUP ***/
void setup() {
  servoController1.startPattern(); // starting the pattern on servoController 1
  servoController2.startPattern(); // starting the pattern on servoController 2
}

/*** LOOP ***/
void loop() {
  /* the 2 servoControllers will loop on their patterns */
  servoController1.loop();
  servoController2.loop();
}
