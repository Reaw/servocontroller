#include <ServoController.h>

//Declaring servo controller, it will take default values (pin0, a sweep pattern and 1 second to reach max speed)
ServoController s = ServoController();

void setup() {
  // Starting the pattern
  s.startPattern();
}

void loop() {
  // The servo controller will loop on the pattern
  s.loop();
}
