## ServoController
A simple Arduino tool for controlling digital servo motors. Provides the possibility to use multiple servos "simultaneously" and to set moving patterns to servos. Uses the default Servo library.

DISCLAIMER : This tool is under development, and has only tested with Arduino MKR 1010 boards running on latest firmware version. The features are still very limited yet. There is no guarantee that this tool will be useful for your project in its current version.

```c++
//Declaring servo controller, it will take default values (servo on pin 0 and a sweep pattern that starts at angle 90)
ServoController s = ServoController();

void setup() {
  // Starting the pattern
  s.startPattern();
}

void loop() {
  // The servo controller will loop on the pattern
  s.loop();
}
```

# TODO :
- [ ] Add possibility not to loop on pattern, or chose how many loops
- [ ] Implement all possible calls from Servo.h (or make servo public)
    - get the current angle of the servo
    - writeMS
- [ ] Create ServoController by giving Servo in constructor
- [ ] Make better examples
- [x] Add control of desired speed
- [ ] Clean code for Arduino API style
- [ ] Improve reset function (default angle choice, or first of pattern)
