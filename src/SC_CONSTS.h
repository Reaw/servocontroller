#ifndef SC_CONSTS_H
#define SC_CONSTS_H
/* Pins */
const unsigned short DEFAULT_FIRST_PIN = 0;
/* Cycles */
const unsigned short DEFAULT_MIN_CYCLE = 100U;
const unsigned short DEFAULT_MAX_CYCLE = 4U;
const unsigned short DEFAULT_TIME_TO_REACH_SPEED = 0;
/* Angles */
const unsigned short DEFAULT_ANGLE = 90;
const unsigned short MIN_ANGLE = 0;
const unsigned short MAX_ANGLE = 180;
/* Patterns */
//TODO: Create patterns

#endif SC_CONSTS_H
