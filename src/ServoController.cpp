/*
  ServoController.h - Library for controlling digital servo motors. Provides the possibility to use multiple servos "simultaneously" and to set moving patterns to servos.
  Created by Philippe Bentegeac (https://philippe.bentegeac.com). December 5, 2019.
  Released into the public domain (LGPL 3).
*/

#include "Arduino.h"
#include "ServoController.h"
#include "utils.h"

/***********************
**** PUBLIC METHODS ****
***********************/

//Default constructor, sets SWEEP_PATTERN as default
ServoController :: ServoController(){
  initID();
  initPin();
  initCycles(DEFAULT_TIME_TO_REACH_SPEED);
  unsigned short sweepPattern[2] = {0, 180};
  setPattern(sweepPattern, 2);
  reset();
}

//Constructor overload for setting pin, sets SWEEP_PATTERN as default
ServoController :: ServoController(int pin) {
  initID();
  setPin(pin);
  initCycles(DEFAULT_TIME_TO_REACH_SPEED);
  unsigned short sweepPattern[2] = {0, 180};
  setPattern(sweepPattern, 2);
  reset();
}

//Constructor overload for setting pin and speedChangeCycle, sets SWEEP_PATTERN as default
ServoController :: ServoController(int pin, unsigned short timeToReachSpeed) {
  initID();
  setPin(pin);
  initCycles(timeToReachSpeed);
  unsigned short sweepPattern[2] = {0, 180};
  setPattern(sweepPattern, 2);
  reset();
}

//Constructor overload for setting pin, speedChangeCycle, and pattern
ServoController :: ServoController(int pin, unsigned short timeToReachSpeed, unsigned short pattern[], size_t _patternSize) {
  initID();
  setPin(pin);
  initCycles(timeToReachSpeed);
  setPattern(pattern, _patternSize);
  reset();
}

//Sets the time to accelerate/slow down to maxCycle/minCycle
//defines speedChangeCycle's value, if 0 is given as parameter, the value will be set as default (1sec)
void ServoController :: setTimeToReachMaxSpeed(unsigned short time){
  if (time <= 0)
    time = DEFAULT_TIME_TO_REACH_SPEED;
  speedChangeCycle = time / (DEFAULT_MIN_CYCLE - DEFAULT_MAX_CYCLE);
}

//Has to be called in main loop
void ServoController :: loop(){
  if (patternIsOn) {
    if (cycleCheck(&lastMillis, cycle))
      makeNextMove();
    if (cycleCheck(&lastSpeedChangeMilli, speedChangeCycle))
      checkSpeedChange();
    checkStopping();
  }
}

//Moves the servomotor of the ServoController to a given angle, remember that the angles of a digital
//servo range from 0 to 180, so any value above 180 will be set at 180
void ServoController :: moveToAngle(unsigned short angle) {
  if (angle > MAX_ANGLE)
    angle = MAX_ANGLE;
  _servo.write(angle);
}

//Sets the pattern of the ServoController and makes sure it is in the bounds
void ServoController :: setPattern(unsigned short pattern[], size_t size){
  _nextDestinationIndex = 0;
  _pattern.clear();
  _patternSize = size;
  //make sure pattern values are not over MAX_ANGLE (180°) (can't be less than 0 though), and add them to pattern
  for (size_t i = 0; i < _patternSize; i++) {
    if (pattern[i] > MAX_ANGLE)
      pattern[i] = MAX_ANGLE;
    _pattern.push_back(pattern[i]);
  }
}

//Starts the moving pattern
void ServoController :: startPattern() {
  patternIsOn = true;
  _stopping = false;
  lastSpeedChangeMilli = millis();
  print("Starting pattern");
}

//Stops the moving pattern
void ServoController :: stopPattern() {
  _stopping = true;
  _stopOrderTimeStamp = millis();
  print("Stopping pattern");
}

//Sets the desired speed in degrees per second
void ServoController :: setDesiredSpeed(unsigned short degreesPerSecond){
  unsigned short cycleInMiliSeconds = 1 / (float)degreesPerSecond * 1000;
  if (cycleInMiliSeconds < DEFAULT_MAX_CYCLE)
    cycleInMiliSeconds = DEFAULT_MAX_CYCLE;
  if (cycleInMiliSeconds > DEFAULT_MIN_CYCLE)
    cycleInMiliSeconds = DEFAULT_MIN_CYCLE;
  _desiredCycle = cycleInMiliSeconds;
}

/************************
**** PRIVATE METHODS ****
************************/

//Defines the ID of the ServoController automatically
void ServoController :: initID() {
  static int nextID = 0;
  _clothMoverID = nextID;
  nextID++;
}

//Defines the pin of the ServoController automatically (starts at pin 0)
//then attaches the servo motor to the pin
void ServoController :: initPin() {
  static int nextPin = DEFAULT_FIRST_PIN;
  _clothMoverPin = nextPin;
  nextPin++;
  attachServo();
}

//Sets the pin of the ServoController to the pin given in parameter
//then attaches the servo motor to the pin
void ServoController :: setPin(int pin) {
  _clothMoverPin = pin;
  attachServo();
}

//Sets the values of cycles and calls to setTimeToReachMaxSpeed() to define the speedChangeCycle
void ServoController :: initCycles(unsigned short timeToReachSpeed){
  setTimeToReachMaxSpeed(timeToReachSpeed);
  cycle = DEFAULT_MIN_CYCLE;
  _desiredCycle = DEFAULT_MAX_CYCLE;
}

void ServoController :: attachServo() {
  _servo.attach(_clothMoverPin);
}

// Checks if the cycle can and has to increase
void ServoController :: checkSpeedChange() {
  //If the pattern is _stopping..
  if (_stopping)
    trySlowingDown();   //.. we slow down
  // else we reach desired spead
  else {
    if (cycle < _desiredCycle)
      trySlowingDown();
    else if (cycle > _desiredCycle)
      tryAccelerating();
  }
}

// Checks if max speed is reached, if not: accelerates
void ServoController :: tryAccelerating() {
  //check if we can increase speed, if yes increase
  if (cycle > DEFAULT_MAX_CYCLE)
    cycle--;
}

// Checks if min speed is reached, if not: slows doyn
void ServoController :: trySlowingDown(){
  //check if we can decrease speed, if yes decrease
  if (cycle < DEFAULT_MIN_CYCLE)
    cycle++;
}

void ServoController :: checkStopping() {
  //If the pattern is _stopping we stop it after the number of seconds needed to reach DEFAULT_MIN_CYCLE
  if(_stopping){
    unsigned short stopDelay = (DEFAULT_MIN_CYCLE - DEFAULT_MAX_CYCLE) * speedChangeCycle;
    if (cycleCheck(&_stopOrderTimeStamp, stopDelay)) {
      print("Pattern stopped");
      patternIsOn = false;
      _stopping = false;
    }
  }
}

//Resets the servomotor of the ServoController to default angle and and stops the pattern
void ServoController :: reset() {
  moveToAngle(DEFAULT_ANGLE);
  patternIsOn = false;
}

//Processes the next move to do and makes it
void ServoController :: makeNextMove() {
  bool goingUp;
  //the reason why we use a  while loop instead of an if condition is to protect
  //against agains patterns that have multiple times the same values in a row (ex: 10, 10, 10)
  while (_servo.read() == _pattern[_nextDestinationIndex])
    updateNextDestinationIndex(); //We reached the destination, so we "process" the next one

  if (_servo.read() > _pattern[_nextDestinationIndex])
    goDown();
  else //(_servo.read() < _pattern[_nextDestinationIndex])
    goUp();
}

//Processes the next step of the pattern we want to go to
void ServoController :: updateNextDestinationIndex(){
  if (_nextDestinationIndex >= _patternSize-1)
    _nextDestinationIndex = 0; //if end of pattern, go back to beginning
  else
    _nextDestinationIndex++;
}

// Reduce current angle by 1
void ServoController :: goDown() {
  moveToAngle(_servo.read() - 1);
}

// Increase current angle by 1
void ServoController :: goUp() {
  moveToAngle(_servo.read() + 1);
}

// Prints a char array with information about the object
void ServoController :: print(char* toPrint){
  Serial.print("ServoController ");
  Serial.print(_clothMoverID);
  Serial.print(" (pin ");
  Serial.print(_clothMoverPin);
  Serial.print(") : ");
  Serial.println(toPrint);
}
