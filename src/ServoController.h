/*
  ServoController.h - Library for controlling digital servo motors. Provides the possibility to use multiple servos "simultaneously" and to set moving patterns to servos.
  Created by Philippe Bentegeac (https://philippe.bentegeac.com). December 5, 2019.
  Released into the public domain (LGPL 3).
*/

#ifndef ServoController_h
#define ServoController_h

#include <SPI.h>
#include <Servo.h>
#include <Array.h>
#include "SC_CONSTS.h"

class ServoController {
  public:
    /* MEMBERS */
    bool patternIsOn = false; //Is the pattern started
    unsigned short cycle; //cycle for moving to next step
    unsigned short lastMillis = 0;
    unsigned short speedChangeCycle; //cycle for accelerating the moving cycle
    unsigned short lastSpeedChangeMilli = 0;

    /* METHODS */
    ServoController();
    ServoController(int pin);
    ServoController(int pin, unsigned short timeToReachSpeed);
    ServoController(int pin, unsigned short timeToReachSpeed, unsigned short pattern[], size_t _patternSize);
    void loop();
    void setPattern(unsigned short pattern[], size_t size);
    void startPattern(); //Starts the moving pattern
    void stopPattern(); //Stops the moving pattern
    void setTimeToReachMaxSpeed(unsigned short time);
    void moveToAngle(unsigned short angle);
    void setDesiredSpeed(unsigned short degreesPerSecond);
    void reset(); //Resets the servomotor of the ServoController to default angle

  private:
    /* MEMBERS */
    int _clothMoverID = 0;
    int _clothMoverPin = 0;
    Servo _servo;
    unsigned short _desiredCycle;
    unsigned short _stopOrderTimeStamp; // time stamp at which the ServoController received the order to stop
    bool _stopping = false; //The Clothmover has been told to stop
    static const int MAX_PATTERN_SIZE = 32; //it's actually more TODO check array lib
    Array<unsigned short int, MAX_PATTERN_SIZE> _pattern;
    size_t _patternSize;
    unsigned short _nextDestinationIndex; // index of the next destination of the servo motor in the pattern

    /* METHODS */
    void initID();
    void initPin();
    void initPin(int pin);
    void initCycles(unsigned short timeToReachSpeed);
    void setPin(int pin);
    void attachServo();

    void makeNextMove();
    void updateNextDestinationIndex();
    void goDown();
    void goUp();

    void checkSpeedChange();
    void checkStopping();
    void tryAccelerating();
    void trySlowingDown();

    void print(char* toPrint);
};

#endif
