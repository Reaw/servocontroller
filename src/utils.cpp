#include "utils.h"

boolean cycleCheck(unsigned short * lastMillis, unsigned int cycle) {
  unsigned short currentMillis = millis();
  if (currentMillis - *lastMillis >= cycle)
  {
    *lastMillis = currentMillis;
    return true;
  }
  else
    return false;
}
